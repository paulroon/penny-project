import React, { Fragment, Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';

import * as ActionCreators from '../store/actions';

import Header from '../components/Header';
import Slider from '../components/Slider';
import Banner from '../components/Banner';
import Footer from '../components/Footer';

import { Home, About, Journeys, Map, AddStory, Donate } from '../components/pages';

class Layout extends Component {

    render() {
        return (
            <Fragment>
                
                <Router>
    
                    <Fragment>
    
                        <Header logoClick={this.props.toggleSliderVisHandler} />
    
                        {this.props.config.showSlider ? <Slider />: <Banner />}
    
                        <Switch>
                            <Route path="/about" component={About} />
                            <Route path="/journeys" component={Journeys} />
                            <Route path="/map" component={Map} />
                            <Route path="/add-story" component={AddStory} />
                            <Route path="/donate" component={Donate} />
                            <Route path="/" exact component={Home} />
                        </Switch>
    
                        <Footer />
    
                    </Fragment>
    
                </Router>
    
                {/* <a href="#home" className="move-top text-center"></a> */}
    
            </Fragment>
        );
    }

};

const mapStateToProps = (state) => {
    return {
        config: state.cfg
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSliderVisHandler: () => dispatch(ActionCreators.toggleSlider())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);