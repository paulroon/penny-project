import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://penny-project-259ac.firebaseio.com/'
});

export default instance;