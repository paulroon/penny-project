
export const HOME = 'HOME';
export const ABOUT = 'ABOUT';
export const JOURNEYS = 'JOURNEYS';
export const MAP = 'MAP';
export const ADD_STORY = 'ADD_STORY';
export const DONATE = 'DONATE';

export const generate = (id) => {
    return routes.find(link => link.id === id).link;
}

export const routes = [
    { id: HOME, link: '/', label: 'Home' },
    { id: ABOUT, link: '/about', label: 'About' },
    { id: JOURNEYS, link: '/journeys', label: 'Journeys' },
    { id: MAP, link: '/map', label: 'Map' },
    { id: ADD_STORY, link: '/add-story', label: '+ a Story' },
    { id: DONATE, link: '/donate', label: 'Donate' }
];