import { detailedDiff } from 'deep-object-diff';

const logger = store => next => action => {

    const oldState = store.getState();
    const result = next(action);

    // Case:: Thunk - handles action as action(dispatch){...}   
    if(typeof action === 'function') {
        return result;
    }

    const diff = detailedDiff(oldState, store.getState());

    const diffOutput = {};
    const grpStyles = 'display: block; width: 100%; padding: 5px; margin: 5px; border: 1px solid #4f4f4f; background-color: #ebebeb; letter-spacing: 0.1em; font-size: 12px; color: #4f4f4f;';
        
    Object.keys(diff)
        .filter(type => Object.keys(diff[type]).length > 0)
        .map(type => {
                diffOutput[type] = {};
                Object.keys(diff[type])
                    .map(deltaPropertyName => {
                    
                        diffOutput[type][deltaPropertyName] = [JSON.stringify(diff[type][deltaPropertyName]), diff[type][deltaPropertyName]]
                    
                    return true;
                });


            return true;
        });
        
    console.groupCollapsed("%c " + action.type, grpStyles)
    
    for(let type in diffOutput) {
        const grpLabel = "%c " + type.toUpperCase();
        
            console.group(grpLabel, grpStyles)
                console.table(diffOutput[type]);
            console.groupEnd();
    }

    console.groupEnd();

    return result;
}

export default logger;