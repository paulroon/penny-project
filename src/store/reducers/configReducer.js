import * as ActionType from '../actions/actionTypes';

const initialState = {
    showSlider: false
}

const configReducer = (state = initialState, action) => {
    const newState = {...state};
    switch (action.type) {
        case ActionType.SHOW_SLIDER:
            newState.showSlider = true;
            break;
        case ActionType.HIDE_SLIDER:
            newState.showSlider = false;
            break;        
        case ActionType.TOGGLE_SLIDER:
        newState.showSlider = !state.showSlider;
        break;
        default:
    }
    return newState;
}

export default configReducer;