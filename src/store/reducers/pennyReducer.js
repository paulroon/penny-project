import * as ActionType from '../actions/actionTypes';

const initialState = {
    pennies: []
}

const pennyReducer = (state = initialState, action) => {
    const newState = {...state};
    switch (action.type) {
        case ActionType.STORE_PENNIES:
            newState.pennies = action.payload.pennies;
            break;
        default:
    }
    return newState;
}

export default pennyReducer;