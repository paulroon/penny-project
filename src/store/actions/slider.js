import * as ActionTypes from './actionTypes';

export const showSlider = () => {
    return { type: ActionTypes.SHOW_SLIDER }
}
export const hideSlider = () => {
    return { type: ActionTypes.HIDE_SLIDER }
}
export const toggleSlider = () => {
    return { type: ActionTypes.TOGGLE_SLIDER }
}
