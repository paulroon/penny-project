import * as ActionTypes from './actionTypes';
import axios from '../../config/axios';


// Action Creators


export const fetchPennies = () => dispatch => {

    axios.get('/pennies.json')
        .then(resp => {
            
            const statePennies = Object.keys(resp.data).map(key => { 
                return { 
                    id: key, 
                    name: resp.data[key]
                };
            });
            
            dispatch(storePennies(statePennies));

        })
        .catch(err => {
            console.error(err);
        });

}

export const storePennies = (p) => {
    return {
        type: ActionTypes.STORE_PENNIES,
        payload: { 
            pennies: p 
        }
    }
}