
// Config Actions
export const SHOW_SLIDER = 'SHOW_SLIDER';
export const HIDE_SLIDER = 'HIDE_SLIDER';
export const TOGGLE_SLIDER = 'TOGGLE_SLIDER';

// Penny Actions
export const STORE_PENNIES = 'STORE_PENNIES';