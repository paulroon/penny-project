export {
    showSlider,
    hideSlider,
    toggleSlider
} from './slider';

export {
    fetchPennies
} from './pennies';