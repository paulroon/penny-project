import React from 'react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import ConfigReducer from './reducers/configReducer';
import PennyReducer from './reducers/pennyReducer';
import Logger from './middleware/Logger';


const rootReducer = combineReducers({
    cfg: ConfigReducer,
    penny: PennyReducer
});

const middleware = [ Logger, thunk ];

const composeEnhancers = (typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ )
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) 
    : compose;

const enhancers = composeEnhancers(
  applyMiddleware(...middleware),
  // other store enhancers if any
);

// Store
const store = createStore(rootReducer, enhancers);

export default (props) => <Provider store={store}>{props.children}</Provider>;