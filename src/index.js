import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ApplcationStoreProvider from './store/ApplicationStoreProvider';

ReactDOM.render(<ApplcationStoreProvider><App /></ApplcationStoreProvider>, document.getElementById('root'));

serviceWorker.unregister();
