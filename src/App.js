import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as ActionCreators from './store/actions';

import Layout from './containers/Layout';

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoins, faBars, faMapMarker, faEnvelopeOpen } from '@fortawesome/free-solid-svg-icons'

import './App.css';

// Font awesome Library mod
//
library.add(faCoins, faBars, faMapMarker, faEnvelopeOpen);


class App extends Component {

  componentDidMount() {
    this.props.fetchPenniesHandler();
  }

  render() {
    return (
      <div className="App">
          <Layout />
      </div>
    );
  }
}

const mapDispatchToProps =(dispatch) => {
  return {
    fetchPenniesHandler: () => dispatch(ActionCreators.fetchPennies())
  };
}

export default connect(null, mapDispatchToProps)(App);
