import React, { useState } from 'react';
import { Link } from "react-router-dom";
import oliveArloJpg from '../assets/img/olive_arlo.jpg';
import coinsJpg from '../assets/img/coins.jpg';
import classes from './Ui.module.css';

const slider = (props) => {

    const [ currentSlideIndex ] = useState(0);
    const [ slides ] = useState([
        {
            id: 1,
            title: 'What is The Penny Project',
            image: coinsJpg,
            sub: 'Keeping an eye on your pennies as the move about.',
            text: 'We\'re guessing you are here because you found a penny with one of our stickers on it. Here you can see where other people have let us know where and when they too saw that exact same penny. Why don\'t you let us know your story and we\'ll do the rest.',
            more: {
                path: 'about'
            }
        },
        {
            id: 2,
            title: 'Who made The Penny Project',
            image: oliveArloJpg,
            sub: 'WE did!!! - Olive and Arlo',
            text: 'We\'re brother and sister who go to steep school in Hampshire. We cooked this idea up in our brains.. so no complaints yeh!',
            more: {
                path: '/about'
            }
        }
    ]);

    const moreButton = (slide) => {
        return (
            <Link to={slide.more.path} className={classes.ReadMoreBtn}>Read more</Link>
        );
    }

    const slideSelection = () => Object.keys(slides).slice(currentSlideIndex, 2).map(index => {
        const currentSlide = slides[index];
        return (
            <div key={index} className="banner-top" style={{ background: 'url('+ currentSlide.image +') no-repeat center'}}>
                <div className="overlay">
                    <div className="container">
                        <div className="w3layouts-banner-info text-center">
                            <h3 className="text-wh">{currentSlide.title}</h3>
                            <h4 className="text-wh mx-auto my-4">{currentSlide.sub}</h4>
                            <p className="text-li mx-auto mt-2">{currentSlide.text}</p>
                            {moreButton(currentSlide)}
                        </div>
                    </div>
                </div>
            </div>
        );
    });


    return (
        <React.Fragment>
        {slideSelection()}
        </React.Fragment>
    );

};

export default slider;
