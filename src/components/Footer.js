import React from 'react';
import { Link } from 'react-router-dom';

import Icon from '../components/ui/Icon';

const footer = (prop) => {
    return (
        <footer className="text-center py-5">
            <div className="container py-md-3">

                <h2 className="logo2 text-center">
                    <Link to={'/'}>
                        <span className="fa fa-coins"></span> Penny Project
                    </Link>
                </h2>

                <div className="contact-left-footer mt-4">
                    <ul>
                        <li>
                            <p>
                                <Icon fa="map-marker" className="mr-2" />Olive &amp; Arlo Rooney
                            </p>
                        </li>
                        <li>
                            <p className="text-wh">
                                <span className="fa fa-envelope-open mr-2"></span>
                                <a href="mailto:info@penny-project.co.uk">info@penny-project.co.uk</a>
                            </p>
                        </li>
                    </ul>
                </div>

                <div className="footercopy-social my-4">
                    <ul>
                        <li>
                            <Link to={'/'}>
                                <span className="fa fa-facebook-square"></span>
                            </Link>
                        </li>
                        <li className="mx-2">
                            <Link to={'/'}>
                                <span className="fa fa-twitter-square"></span>
                            </Link>
                        </li>
                        <li className="">
                            <Link to={'/'}>
                                <span className="fa fa-google-plus-square"></span>
                            </Link>
                        </li>
                        <li className="mx-2">
                            <Link to={'/'}>
                                <span className="fa fa-linkedin-square"></span>
                            </Link>
                        </li>
                        <li>
                            <Link to={'/'}>
                                <span className="fa fa-rss-square"></span>
                            </Link>
                        </li>
                        <li className="ml-2">
                            <Link to={'/'}>
                                <span className="fa fa-pinterest-square"></span>
                            </Link>
                        </li>
                    </ul>
                </div>

            </div>
        </footer>
    );
};

export default footer;