import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const icon = (props) => <FontAwesomeIcon className="fa-icon" icon={props.fa} {...props} />;

export default icon;