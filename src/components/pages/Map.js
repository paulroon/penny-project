import React from 'react';

const map = (prop) => {
    return (
        <section className="background-img">
            <div className="overlay-clr py-5">
                <div className="container py-md-3">
                    <div className="row core-grids">
                        <div className="col-lg-4 bg-left">	
                            <h4 className="">Map Stuff....</h4>
                        </div>
                        <div className="col-lg-5 col-md-7 bg-middle mt-lg-0 mt-4">	
                            <p className="">Integer sit amet mattis quam, sit amet dol ultricies velit. Praesent ullam corper duits turpis dolor sit amet quam.
                            Nulla comodol gravida porttitor. Aenean posuere lacusnt quis leo imperdiet laoreet. Proin vulputat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default map;