import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as ActionCreators from '../../store/actions';

class Home extends Component {

    componentDidMount() {
        this.props.showSliderHandler()
    }

    componentWillUnmount() {
        this.props.hideSliderHandler()
    }

   render() {
       return (
            <section className="blog py-5">
                <div className="container py-md-5">
                    <h3 className="heading mb-sm-5 mb-4 text-center"> Our Big Idea</h3>
                    <div className="row blog-grids">
                        <div className="col-lg-4 col-md-6 blog-left mb-lg-0 mb-sm-5 pb-lg-0 pb-5">	
                            <img src="images/s1.jpg" className="img-fliud" alt="" />
                            <div className="blog-info">
                                <h4>Pennies <span className="fa fa-pagelines"></span></h4>
                                <p className="mt-2">Integer sit ut amet mattis quam, sit amet ultricies velit. Praesent ullam corper dui turpis sit.</p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 blog-middle mb-lg-0 mb-sm-5 pb-lg-0 pb-md-5">	
                            <img src="images/s2.jpg" className="img-fliud" alt="" />
                            <div className="blog-info">
                                <h4>Information <span className="fa fa-pagelines"></span></h4>
                                <p className="mt-2">Integer sit ut amet mattis quam, sit amet ultricies velit. Praesent ullam corper dui turpis sit.</p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 blog-right mt-lg-0 mt-5 pt-lg-0 pt-md-5">
                            <img src="images/s3.jpg" className="img-fliud" alt="" />
                            <div className="blog-info">
                                <h4>Charity <span className="fa fa-pagelines"></span></h4>
                                <p className="mt-2">Integer sit ut amet mattis quam, sit amet ultricies velit. Praesent ullam corper dui turpis sit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
   }

};

const mapDispatchToProps = (dispatch) => {
    return {
        showSliderHandler: () => dispatch(ActionCreators.showSlider()),
        hideSliderHandler: () => dispatch(ActionCreators.hideSlider()),
    }
}

export default connect(null, mapDispatchToProps)(Home);