import React from 'react';

const donate = (prop) => {
    return (
        <section className="text py-5">
            <div className="container py-md-3 text-center">		
                <div className="row">
                    <div className="col-12">
                        <h3 className="mb-4 heading">The Best Time To <span>Donate</span> Is Now.</h3>
                        <p>We believe that our ability to spend what we have, the way we do is a privilage. We hope that you have enjoyed our little experiment. Perhaps you would also consider making a small (or large <span role="img" aria-label="thinking emoji">🤔</span>) donation to our favorite charity, Wateraid.</p>
                        <p>we're setting up a justgiving page really soon so we can count how nice everyone is being.</p>
                        <a href="https://www.wateraid.org/uk/" className="btn mr-3"> Wateraid Info</a>
                        <a href="https://www.wateraid.org/uk/donate" className="btn btn1"> Donate </a>
                    </div>		
                </div>		
            </div>		
        </section>
    );
};

export default donate;