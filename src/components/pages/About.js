import React from 'react';

const about = (prop) => {
    return (
        <section className="about py-5">
            <div className="container py-md-4">
                <h3 className="heading text-center mb-4">Not a School Project</h3>
                <p className="about-text mx-auto text-center">Tempor Ut enim ad minim quis nostrud exerci sed do eiusmod tempor incididunt ut
                    labore et dolore magna aliqua nostrud exerci sed nostrud exerci ipsum dolor ut.</p>
                <div className="feature-grids row mt-5 text-center">
                    <div className="col-lg-4 col-md-6 ">
                        <div className="bottom-gd px-2 text-center">
                            <div className="f-icon">
                                <span className="fa fa-coins" aria-hidden="true"></span>
                            </div>
                            <h3 className="mt-4"> Coins</h3>
                            <p className="mt-3">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet quam.</p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 mt-md-0 mt-4">
                        <div className="bottom-gd px-2 text-center">
                            <div className="f-icon">
                                <span className="fa fa-apple" aria-hidden="true"></span>
                            </div>
                            <h3 className="mt-4"> Coins</h3>
                            <p className="mt-3">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet quam.</p>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 mt-lg-0 mt-4">
                        <div className="bottom-gd px-2 text-center">
                            <div className="f-icon">
                                <span className="fa fa-pagelines" aria-hidden="true"></span>
                            </div>
                            <h3 className="mt-4">Coins</h3>
                            <p className="mt-3">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet quam.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default about;