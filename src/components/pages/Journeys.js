import React from 'react';
import { connect } from 'react-redux';

import classes from './Pages.module.css';


const journeys = (props) => {

    return (
        <section className="core-value py-5">
            <div className="container py-md-4">
                <h3 className="heading mb-sm-5 mb-4 text-center"> Our Penny Journies</h3>
                <div className="row core-grids">
                    <div className="col-lg-6 core-left">	
                        <img src="images/core.png" className="img-fliud" alt="" />
                    </div>
                    <div className="col-lg-6 core-right">
                        <h4 className="mt-4">Pennies Move about.</h4>
                        <p className="mt-3">Select a Penny and see where it has been.</p>
                        <ul>
                            { props.pennies.map(penny => <li key={penny.id} onClick={() => alert('todo: pennyId: ' + penny.id)} className={classes.PennySelector}>{penny.name}</li>)}
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    );
};

const mapStateToProps = (state) => {
    return {
        pennies: state.penny.pennies
    }
}

export default connect(mapStateToProps)(journeys);