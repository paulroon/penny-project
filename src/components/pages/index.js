import home from './Home';
import about from './About';
import journeys from './Journeys';
import map from './Map';
import addStory from './AddStory';
import donate from './Donate';

import './PageStyles.css';

export const Home = home;
export const About = about;
export const Journeys = journeys;
export const Map = map;
export const AddStory = addStory;
export const Donate = donate;