import React from 'react';
import { Link, withRouter } from "react-router-dom";
import classes from './Ui.module.css';
import * as routeManager from '../config/routes';

import Icon from '../components/ui/Icon';

const header = (props) => {

    return (
        <header>
            <div className="container">

                <nav className="py-4 d-lg-flex">
                    <div id="logo" onClick={props.logoClick}>
                        <h1> <Link to="/"><Icon fa="coins" /> Penny Project</Link></h1>
                    </div>
                    <label htmlFor="drop" className="toggle"><Icon fa="bars" /></label>
                    <input type="checkbox" onChange={() => console.log('changing this value')} id="drop" />
                
                    <ul className="menu mt-md-2 ml-auto">
                        { routeManager.routes.map((link, key) => 
                            (
                                <li key={key} className="mr-lg-4 mr-2">
                                <Link className={props.location.pathname === link.link ? classes.ActiveNavLink: null} to={link.link}>
                                    {link.label}
                                </Link>
                                </li>
                            )
                        )}
                    </ul>
                </nav>
                
            </div>
        </header>
    );
};

export default withRouter(header);