import React from 'react';
import styles from './Button.module.css';

const button = (props) => {
    return (
        <button 
            className={[styles.Button, styles[props.btnType], props.isDisabled?styles.Disabled:styles.Active].join(' ')} 
            onClick={props.clicked}
            disabled={props.isDisabled}
        >{props.children}</button>
    );
};

export default button;