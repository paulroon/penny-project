import React, { Component } from 'react';
import { connect } from 'react-redux';
import classes from './AddStoryForm.module.css';
import Button from '../forms/Button/Button';
import Input from '../forms/Input/Input';
import Spinner from '../ui/Spinner/Spinner';
import axios from '../../config/axios';

class AddStoryForm extends Component {

  fSetFormEl = (t, c, v, l, rules) => {
    return { 
      elementType: t, 
      elementConfig: c, 
      value: v, 
      label: l, 
      valid: false, 
      lastError: rules && rules.message ? rules.message : "Not Valid", 
      touched: false, 
      validationRules: rules 
    };
  };

  state = {
    addStoryForm: {
      name: this.fSetFormEl('input', { type: 'text', placeholder: 'Roonil Wazlib' }, false, "Name: ", { required: true, message: "Name must not be Blank."} ),
      location: this.fSetFormEl('input', { type: 'text', placeholder: 'Charity Shop' }, false, "Where: ", { required: true, message: "must not be Blank"}),
      eventType: this.fSetFormEl('select', { options: [
        { value: 'spending', displayValue: 'Spending or Donating' },
        { value: 'change', displayValue: 'Getting Change' },
        { value: 'recieving', displayValue: 'Receiving' }
      ] }, false, "What were you doing with the Penny: ")
    },
    loading: false
  }


  addStoryPostHandler = (e) => {
    e.preventDefault();

     this.setState({ loading: true });

    const formData = {};
    for(let formDataItem in this.state.addStoryForm) {
        formData[formDataItem] = this.state.addStoryForm[formDataItem].value;
    }
    const story = {
        penny: 'penny_id',
        formData: formData
    };

    // Save the Orders to FireBase
    axios.post('/stories.json', story)
        .then(response => {
            this.setState({ loading: false }); 
            this.props.history.push('/');
        })
        .catch(error => {
            this.setState({ loading: false });
        });

  }

  isValid = (test, rules) => {
    let isValid = true;

    for(let ruleName in rules) {

      let ruleValue = rules[ruleName].condition ? rules[ruleName].condition : rules[ruleName];

      let ruleIsValid = true;
      switch (ruleName.toLowerCase()) {
        case 'required':
          ruleIsValid = (test.trim() !== '');
          break;
        case 'minlen':
          ruleIsValid = (test.trim().length >= ruleValue);
          break;
        case 'maxlen':
          ruleIsValid = (test.trim().length <= ruleValue);
          break;
        default:
      }

      // console.log(test, ruleName, ruleValue, ruleIsValid, ruleErrMessage);
      
      isValid = isValid && ruleIsValid;
    }

    return isValid;
  }

  inputChangeHandler = (event, name) => {

    const newForm = { ...this.state.addStoryForm };
    const newFormEl = { ...newForm[name] };
    newFormEl.value = event.target.value;
    newFormEl.touched = true;
    newFormEl.valid = this.isValid(event.target.value, newFormEl.validationRules);
    newForm[name] = newFormEl;

    this.setState({ addStoryForm: newForm });
  }

  render() {

    let formIsInValidState = true;
    for(let field in this.state.addStoryForm) {
      formIsInValidState = formIsInValidState && (this.state.addStoryForm[field].valid || (!this.state.addStoryForm[field].validationRules))
    }

    const formElements = [];
    for(let formEl in this.state.addStoryForm) {
      formElements.push({
        name: formEl,
        ...this.state.addStoryForm[formEl]
      });
    }

    const formFields = formElements.map(field => {
      return (
        <Input 
          invalid={!field.valid} 
          errorMessage={field.lastError}
          shouldValidate={field.validationRules}
          touched={field.touched}
          key={field.name} 
          changeHandler={(e) => this.inputChangeHandler(e, field.name)} 
          config={field} 
          label={field.label}  
        />)
    });

    return (
      <div className={classes.AddStoryForm}>
        <h2>Your Story</h2>
        {
            this.state.loading 
            ? 
                <Spinner />
            : 
                <form onSubmit={this.addStoryPostHandler}>
                    {formFields}
                    <Button btnType='Success' isDisabled={!formIsInValidState}>Order</Button>
                </form>
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    pennies: state.penny.pennies
  }
}

export default connect(mapStateToProps)(AddStoryForm);;