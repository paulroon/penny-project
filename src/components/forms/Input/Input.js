import React from 'react';
import styles from './Input.module.css';

const input = (props) => {

    let inputElement = null;

    let {elementConfig, elementType, name, value } = props.config;

    if(value === false) {
        value = '';
    }

    const inputClasses = [styles.InputElement];
    let validationErrorMessage = null;
    if(props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push(styles.Invalid);
        validationErrorMessage = <div className={styles.ErrorMessage}>{props.errorMessage}</div>;
    }

    switch (elementType) {
        case ( 'input' ):
            inputElement = <input className={inputClasses.join(' ')} onChange={props.changeHandler} {...elementConfig} name={name} value={value} />
            break;
        case ( 'textarea' ):
            inputElement = <textarea className={inputClasses.join(' ')} onChange={props.changeHandler} {...elementConfig}>{value}</textarea>
            break;
        case ( 'select' ):
            inputElement = <select className={inputClasses.join(' ')} onChange={props.changeHandler}>{
                elementConfig.options.map(opt => <option key={opt.value} value={opt.value}>{opt.displayValue}</option>)
            }</select>
            break;           
        default:
            inputElement = <input className={inputClasses.join(' ')} onChange={props.changeHandler} {...elementConfig} name={name} value={value} />

    }

    return (    
        <div className={styles.Input}>
            <label className={styles.Label}>{props.label}</label>
            {inputElement}
            {validationErrorMessage}
        </div>
    );
};

export default input;